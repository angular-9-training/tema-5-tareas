import { Component, OnInit } from '@angular/core';
import { Listado } from 'src/app/models/listado';
import { Todo } from 'src/app/models/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListComponent implements OnInit {

  listado: Listado;
  textoNuevo: string;

  constructor() { 

    this.listado = new Listado();
  }

  ngOnInit(): void {
  }

  // Método para Agregar un nuevo Todo al listado
  crearNuevaTarea() {
    this.listado.agregar(this.textoNuevo);
  }

  // Método para borrar una Tarea. Este método será llamado cuando en el hijo
  // (TodoComponent) emita el evento de borrar
  borrarTarea(event: any) {
    const tareaABorrar: Todo = event as Todo;
    this.listado.borrar(tareaABorrar);
  }

}

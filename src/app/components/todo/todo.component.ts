import { Component, OnInit , Input, Output, EventEmitter } from '@angular/core';
import { Todo } from 'src/app/models/todo';
import { BrowserTransferStateModule } from '@angular/platform-browser';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.sass']
})
export class TodoComponent implements OnInit {

  // Todo recibido desde el component padre para representar en la vista
  @Input() todo: Todo;

  // Eventos enviados desde el hijo hacia el padre
  @Output() todoABorrar = new EventEmitter<Todo>();

  constructor() { }

  ngOnInit(): void {
  }

  // Método encargado de emitir el Todo hacia el padre para que lo borre de la lista
  borrar(){
    this.todoABorrar.emit(this.todo);
  }

  // Método para cambiar el estado del todo
  cambiarEstado() {
    this.todo.cambiarEstado();
  }

  // Método encargado de cambiar ele stilo del todo según el estado
  // Verde --> Completo
  // Rojo --> Incompleto
  get_colorTarea(): string {
    return this.todo.estado ? 'green' : 'red';
  }

}

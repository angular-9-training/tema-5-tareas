import { Todo } from './todo';

export class Listado {
  listado: Todo[] = [];

  constructor() {

  }

  // Método para añadir todos al listado
  agregar(nombre: string): void {
    const tarea = new Todo(nombre, false);
    this.listado.push(tarea);
  }

  // Método para eliminar todo del listado
  borrar(todo: Todo): boolean {
    let index: number;

    // Obtenemos posición de la tarea en el listado
    index = this.listado.indexOf(todo);
    // Si lo ha encontrado
    if (index >= 0) {
      this.listado.splice(index, 1);
      return true;
    } else {
      return false;
    }
  }
}

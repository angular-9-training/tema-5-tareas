export class Todo {

  nombre: string;
  estado: boolean;


  constructor(nombre: string, estado: boolean) {
    this.nombre = nombre;
    this.estado = estado;
  }

  cambiarEstado() {
    this.estado = !this.estado;
  }

}
